#pragma once 

#include <string>

namespace Utils{

    int read_int_input(const std::string &input_string, const std::string &error_string);
    double read_double_input(const std::string &input_string, const std::string &error_string);

    std::string read_string_input(const std::string &input_string, const std::string &error_string);

}