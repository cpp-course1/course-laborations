#pragma once

#include <string>
#include <map>

namespace filehandler {
    
    template<typename T>
    void writeListToFile(std::string filename, T* list, int length);

    void writeListToFile(std::string filename, double* list, int length);

    void writeToFile(std::string filename, double* doubleArray);

    void printStatisticsOfFile(std::string filename);    

    std::string getAbsolutePath(std::string filename);

    int getNumberOfCharacters(std::string filename);

    int getNumberOfNumbers(std::string filename);

    bool isANumber(std::string number);

    int getNumberOfWords(std::string filename);

    int getNumberOfLines(std::string filename);

    std::map<std::string, int> getWordsCounter(std::string filename);
}

