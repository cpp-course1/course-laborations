#pragma once

namespace mathutils{

    template<typename T>
    double mean(T list[], int length);

    template<typename T>
    double meanRecursive(T list[], int start, int end);

    template<typename T>
    double std(T list[], int length);

    template<typename T>
    T max(T list[], int length);

    template<typename T>
    T min(T list[], int length);

}