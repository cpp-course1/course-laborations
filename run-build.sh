#!/bin/bash
LIB_DIR="./lib"
if [ -d "$LIB_DIR" ]; then
    echo "Dummy text"
else
    echo "libraries does not exist"
    mkdir $LIB_DIR
    cd $LIB_DIR
    git clone https://github.com/google/googletest.git
    cd ..
fi

BUILD_DIR="./build"
if [ -d "$BUILD_DIR" ]; then
    echo "Build directory already exists, skipping creating of dir"
else
    mkdir build
    cp ./include/config/config_base.h ./include/config/config.h
fi

cd build
cmake ..
make
make test