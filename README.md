# Course laborations

This is a gitlab project for all the laborations in the course


# Pre requisits
CMake
'sudo apt-get install cmake'

googletest
cd libs 



# Compilation
###  Use cmake to create make file 
### and then compile with the make command
### The CMakeLists.txt file was created using 
#### https://medium.com/heuristics/c-application-development-part-3-cmakelists-txt-from-scratch-7678253e5e24
mkdir build   
cd build    
cmake ..   
make    


# Run
cd build
./main


# code structure
root          
  | ---- CMakeLists.txt             
  | ---- .gitignore   
  | ---- README.md   
  |   
  ---- src     
  |     | ---- Chapter (s)    
  |             | ---- Sub_chapter (s)
  |                     | ---- lab-<number>      
  |                             | --- <lab files>          
  |         
  ---- include   
  |     | ----- compute    
  |             | ----- operators.h     
  |   
  ---- tests     
  |   
  ---- libs    


# For unit testing with googletest use 
#### https://raymii.org/s/tutorials/Cpp_project_setup_with_cmake_and_unit_tests.html
After compiling  run
'make test'
to run all the tests

or 'find ./tests -executable -type f -exec {} \;' 


# For libraries
# https://cmake.org/examples/


# SCRIPTS
execute './run-build.sh' to run build and test
execute './run-tests.sh' to run all tests


