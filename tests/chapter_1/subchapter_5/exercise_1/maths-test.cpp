#include "gtest/gtest.h"

#include <string>
#include "maths.h"

TEST(geometricProgression, testNegativeN){
    try{
        functions::geometric_progression(1,1,-1);
        FAIL();
    }
    catch (const std::invalid_argument& e){
        ASSERT_STREQ(std::string(e.what()).c_str(), "n IS SMALLER THAN 0, UNABLE TO FINISH SQUARE");
    }
}

TEST(geometricProgression, testValues){
    double delta = 1e-6;
    double result = functions::geometric_progression(1,1,1);
    double expected = 1;
    ASSERT_NEAR(result, expected, delta);
}