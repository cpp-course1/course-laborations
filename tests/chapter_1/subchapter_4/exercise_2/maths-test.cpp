#include "gtest/gtest.h"

#include <string>
#include "maths.h"

TEST(mathsFunctions, testNegativeSQRT){
    try{
        functions::sqrt_formula(1,1.9,1);
        FAIL();
    }
    catch (const std::invalid_argument& e){
        ASSERT_STREQ(std::string(e.what()).c_str(), "b^2 IS SMALLER THAN 4ac, UNABLE TO FINISH SQUARE");
    }
}


TEST(mathsFunctions, testZeroSQRT){
    try{
        functions::sqrt_formula(1,2,1);
    }
    catch (const std::invalid_argument& e){
        FAIL();
    }
}

TEST(mathsFunctions, testPositiveSQRT){
    try{
        functions::sqrt_formula(1,2.1,1);
    }
    catch (const std::invalid_argument& e){
        FAIL();
    }
}


TEST(mathsFunctions, testValues){
    double delta = 1e-6;
    double result = functions::sqrt_formula(1,2,1);
    double expected = -1;
    ASSERT_NEAR(result, expected, delta);

    
    result = functions::sqrt_formula(1,3,1);
    expected = -0.381966;
    ASSERT_NEAR(result, expected, delta);
}


TEST(mathsFunctions, testZeroA){
    double delta = 1e-6;
    double result = functions::sqrt_formula(0,1,1);
    double expected = -1;
    ASSERT_NEAR(result, expected, delta);
}