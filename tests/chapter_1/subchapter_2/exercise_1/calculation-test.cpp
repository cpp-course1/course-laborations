#include "gtest/gtest.h"
#include "calculation.h"


TEST(calculations, First){
    int result = calculations::first();

    // to be calculatied by user 
    int expected = 7; 
    ASSERT_EQ(result, expected);
}

TEST(calculations, Second){
    int result = calculations::second(7);

    // to be calculatied by user 
    int expected = -11; 
    ASSERT_EQ(result, expected);

}
