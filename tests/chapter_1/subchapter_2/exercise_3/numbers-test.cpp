#include "gtest/gtest.h"
#include "numbers.h"

TEST(countNumbers, test1){
    int result = numberutils::count_digits(1);
    ASSERT_EQ(result, 1);
}

TEST(countNumbers, test12){
    int result = numberutils::count_digits(12);
    ASSERT_EQ(result, 2);
}

TEST(countNumbers, test123){
    int result = numberutils::count_digits(123);
    ASSERT_EQ(result, 3);
}

TEST(countNumbers, test0){
    int result = numberutils::count_digits(0);
    ASSERT_EQ(result, 1);
}

TEST(countNumbers, testNegative){
    int result = numberutils::count_digits(-1);
    ASSERT_EQ(result, 1);
}

TEST(countNumbers, testNegativeBIG){
    int result = numberutils::count_digits(INT32_MIN);
    ASSERT_EQ(result, 10);
}

TEST(countNumbers, testBIG){
    int result = numberutils::count_digits(INT32_MAX);
    ASSERT_EQ(result, 10);
}