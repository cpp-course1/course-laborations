#include "gtest/gtest.h"
#include "numbers.h"

#include <sstream>
#include <iostream>
#include <streambuf>
#include <string>

TEST(printNumbers, testVariousDevisorOutputs){
    //arrange
    /* Redirect stdout and stderr*/
    std::stringstream stdoutBuffer;
    std::stringstream stderrBuffer;

    std::streambuf *stdoutBuff = std::cout.rdbuf();
    std::streambuf *stderrBuff = std::cerr.rdbuf();
    
    std::cout.rdbuf(stdoutBuffer.rdbuf());
    std::cerr.rdbuf(stderrBuffer.rdbuf());

    std::string testString;
    /* Setup testing  */

    //act

    //assert

    numberutils::print_all_divisors(1);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1");
   
    numberutils::print_all_divisors(2);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 2");


    numberutils::print_all_divisors(3);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 3");

    numberutils::print_all_divisors(4);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 2, 4");

    numberutils::print_all_divisors(5);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 5");

    numberutils::print_all_divisors(6);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 2, 3, 6");

    numberutils::print_all_divisors(7);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 7");

    numberutils::print_all_divisors(8);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 2, 4, 8");

    numberutils::print_all_divisors(9);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 3, 9");

    numberutils::print_all_divisors(10);
    std::getline(stdoutBuffer, testString);
    ASSERT_STREQ(testString.c_str(), "1, 2, 5, 10");

    // teardown

    std::cout.rdbuf(stdoutBuff);
    std::cerr.rdbuf(stderrBuff);
}

TEST(divisorNumbers, testVariousDivisorsPositive){
    ASSERT_TRUE(numberutils::is_divisor(10, 5));
    ASSERT_TRUE(numberutils::is_divisor(20, 5)); 
    ASSERT_TRUE(numberutils::is_divisor(2, 1)); 

}


TEST(divisorNumbers, testAllPositiveToHundredDivisibleOne){
    for (int i = 1; i <= 100; i++){
        ASSERT_TRUE(numberutils::is_divisor(i, 1)); 
    }
}

TEST(divisorNumbers, testAllPositiveToHundredDivisibleTwo){
    for (int i = 1; i <= 100; i+=2){
        ASSERT_FALSE(numberutils::is_divisor(i, 2)); 
        ASSERT_TRUE(numberutils::is_divisor(i+1, 2)); 
    }
}

TEST(divisorNumbers, testNegativeNumbersToNegHundred){
    for (int i = -1; i >= 100; i--){
        ASSERT_TRUE(numberutils::is_divisor(i, 1)); 
        ASSERT_TRUE(numberutils::is_divisor(i, 2)); 
        ASSERT_TRUE(numberutils::is_divisor(i, 3)); 
    }
}


TEST(divisorNumbers, testNegativeDivisorsToNegHundred){
    for (int i = -1; i >= 100; i--){
        ASSERT_FALSE(numberutils::is_divisor(1, i)); 
        ASSERT_FALSE(numberutils::is_divisor(2, i)); 
        ASSERT_FALSE(numberutils::is_divisor(3, i)); 
    }
}

TEST(divisorNumbers, testDivideSelfUpToHundred){
    for (int i = 1; i <= 100; i++){
        ASSERT_TRUE(numberutils::is_divisor(i, i)); 
    }
}