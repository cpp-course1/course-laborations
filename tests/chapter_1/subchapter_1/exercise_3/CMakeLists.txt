# Set binary test file
set(EXERCISE exercise-1_1_3)
set(REL_PATH chapter_1/subchapter_1/exercise_3)
set(REL_REV_PATH ../../../)

set(BINARY ${EXERCISE}-test)

include_directories( ${REL_REV_PATH}../src/${REL_PATH})

# find all source and include files from this folder, its recursive
file(GLOB_RECURSE TEST_SOURCES LIST_DIRECTORIES false *.h *.cpp )

# set the files to a source  variable
set(SOURCES ${TEST_SOURCES})

message(${SOURCES})

add_executable(${BINARY} ${TEST_SOURCES})

add_test(NAME ${BINARY} COMMAND ${BINARY})

target_link_libraries(${BINARY} PUBLIC ${EXERCISE}_lib gtest)