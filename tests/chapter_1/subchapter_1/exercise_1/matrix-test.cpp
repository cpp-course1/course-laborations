#include "gtest/gtest.h"
#include "matrix.h"

#include <sstream>
#include <iostream>
#include <streambuf>
#include <string>

TEST(printMatrixTest, Default){
    //arrange
    /* Redirect stdout and stderr*/
    std::stringstream stdoutBuffer;
    std::stringstream stderrBuffer;

    std::streambuf *stdoutBuff = std::cout.rdbuf();
    std::streambuf *stderrBuff = std::cerr.rdbuf();
    
    std::cout.rdbuf(stdoutBuffer.rdbuf());
    std::cerr.rdbuf(stderrBuffer.rdbuf());

    /* Setup testing matrix */
    printmatrix::Matrix mat;

    //act
    mat.print();

    //assert
    std::string testString;
    std::getline(stdoutBuffer, testString);
    ASSERT_EQ(testString.length(), 1);
    ASSERT_STREQ(testString.c_str(), ".");
    std::getline(stdoutBuffer, testString);
    ASSERT_EQ(testString.length(), 0);

    // teardown

    std::cout.rdbuf(stdoutBuff);
    std::cerr.rdbuf(stderrBuff);
}

TEST(printMatrixTest, NegativeColumns){
    //arrange
    /* Redirect stdout and stderr*/
    std::stringstream stdoutBuffer;
    std::stringstream stderrBuffer;

    std::streambuf *stdoutBuff = std::cout.rdbuf();
    std::streambuf *stderrBuff = std::cerr.rdbuf();
    
    std::cout.rdbuf(stdoutBuffer.rdbuf());
    std::cerr.rdbuf(stderrBuffer.rdbuf());

    /* Setup testing matrix */
    printmatrix::Matrix mat;

    //act
    mat.setColumns(-1);
    mat.setRows(3);
    mat.print();

    //assert
    ASSERT_STREQ(stdoutBuffer.str().c_str(), "\n\n\n");

    // teardown

    std::cout.rdbuf(stdoutBuff);
    std::cerr.rdbuf(stderrBuff);
}



TEST(printMatrixTest, NegativeRows){
    //arrange
    /* Redirect stdout and stderr*/
    std::stringstream stdoutBuffer;
    std::stringstream stderrBuffer;

    std::streambuf *stdoutBuff = std::cout.rdbuf();
    std::streambuf *stderrBuff = std::cerr.rdbuf();
    
    std::cout.rdbuf(stdoutBuffer.rdbuf());
    std::cerr.rdbuf(stderrBuffer.rdbuf());

    /* Setup testing matrix */
    printmatrix::Matrix mat;

    //act
    mat.setColumns(100);
    mat.setRows(-1);
    mat.print();

    //assert
    ASSERT_STREQ(stdoutBuffer.str().c_str(), "");

    // teardown

    std::cout.rdbuf(stdoutBuff);
    std::cerr.rdbuf(stderrBuff);
}

TEST(printMatrixTest, TenByTen){
    //arrange
    /* Redirect stdout and stderr*/
    std::stringstream stdoutBuffer;
    std::stringstream stderrBuffer;

    std::streambuf *stdoutBuff = std::cout.rdbuf();
    std::streambuf *stderrBuff = std::cerr.rdbuf();
    
    std::cout.rdbuf(stdoutBuffer.rdbuf());
    std::cerr.rdbuf(stderrBuffer.rdbuf());

    /* Setup testing matrix */
    printmatrix::Matrix mat;

    //act
    mat.setColumns(10);
    mat.setRows(10);
    mat.print();

    //assert
    std::string testString;
    for (int i = 0;  i < 5; i++){
        std::getline(stdoutBuffer,testString);
        ASSERT_STREQ(testString.c_str(), ".*.*.*.*.*");
        std::getline(stdoutBuffer,testString);
        ASSERT_STREQ(testString.c_str(), "*.*.*.*.*.");
    }

    // teardown

    std::cout.rdbuf(stdoutBuff);
    std::cerr.rdbuf(stderrBuff);
}