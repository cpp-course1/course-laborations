#include "gtest/gtest.h"

#include "utilities/mathUtils.h"
#include <string>
#include "randoms.h"

#define LARGE_NUMBER 1000000
#define ACCEPTABLE_DOUBLE_DELTA 0.0001

TEST(gaussian, testDefaultMean){
    randomizations::Gaussian gaussian;
    double result = gaussian.getMean();
    double expected = 0;

    EXPECT_EQ(result, expected);
}


TEST(gaussian, testDefaultStd){
    randomizations::Gaussian gaussian;
    double result = gaussian.getStd();
    double expected = 1;

    EXPECT_EQ(result, expected);
}


TEST(gaussian, testSetStd){
    randomizations::Gaussian gaussian;
    double expected = 10.356;
    gaussian.setStd(expected);
    double result = gaussian.getStd();
    EXPECT_EQ(result, expected);
}


TEST(gaussian, testSetMean){
    randomizations::Gaussian gaussian;
    double expected = 10.356;
    gaussian.setMean(expected);
    double result = gaussian.getMean();
    EXPECT_EQ(result, expected);
}

TEST(poisson, testDefaultLambda){
    randomizations::Poisson poisson;
    double result = poisson.getLambda();
    double expected = 1;
    EXPECT_EQ(result, expected);
}


TEST(poisson, testSetLambda){
    randomizations::Poisson poisson;
    double expected = 10.5;
    poisson.setLambda(expected);
    double result = poisson.getLambda();
    EXPECT_EQ(result, expected);
}

