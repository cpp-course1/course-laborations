#include "gtest/gtest.h"

#include "function.h"

#include <sstream>
#include <iostream>
#include <streambuf>
#include <string>

#define DOUBLE_ACCEPTABLE_DELTA 0.00001

TEST(derivates, testZeroFunction){
    double derivate = function::derivate<double>([](double d)->double{return 0;}, 0.0);

    double expected = 0;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);
}


TEST(derivates, testConstantFunction){
    double derivate = function::derivate<double>([](double d)->double{return d;}, 0);

    double expected = 1.0;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);
}

TEST(derivates, testConstantFunctionHighSlope){
    #define SLOPE 123456789.0
    double derivate = function::derivate<double>([](double d)->double{return SLOPE*d;}, 0.0);

    double expected = SLOPE;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);
}

TEST(derivates, testConstantFunctionSmallSlope){
    #define SLOPE 123456789.0
    double derivate = function::derivate<double>([](double d)->double{return SLOPE*d;}, 0.0);

    double expected = SLOPE;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);
}


TEST(derivates, testQuadraticFunction){
    double derivate = function::derivate<double>([](double d)->double{return d*d;}, 0.0);

    double expected = 0;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);


    
    derivate = function::derivate<double>([](double d)->double{return d*d;}, 1.0);

    expected = 2.0;
    ASSERT_NEAR(derivate, expected, DOUBLE_ACCEPTABLE_DELTA);
}

/*
TEST(palindrome, testObviousTrue){
    std::string testString = "aa";
    bool result = stringhandler::isPalindrome(testString);

    bool expected = true;
    ASSERT_EQ(result, expected);
}


TEST(palindrome, testEmpty){
    std::string testString = "";
    bool result = stringhandler::isPalindrome(testString);

    bool expected = true;
    ASSERT_EQ(result, expected);
}

TEST(palindrome, testLongTrue){
    std::string testString = "abcdefghiihgfedcba";
    bool result = stringhandler::isPalindrome(testString);

    bool expected = true;
    ASSERT_EQ(result, expected);
}

TEST(palindrome, testLongFalse){
    std::string testString = "abcdefghiihgfedcbaa";
    bool result = stringhandler::isPalindrome(testString);

    bool expected = false;
    ASSERT_EQ(result, expected);
}

TEST(palindrome, testWithNumbers){
    std::string testString = "123321";
    bool result = stringhandler::isPalindrome(testString);

    bool expected = true;
    ASSERT_EQ(result, expected);
}*/