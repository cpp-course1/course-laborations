#include "gtest/gtest.h"
#include "complex.h"

#include <sstream>
#include <iostream>
#include <streambuf>
#include <string>

#define DOUBLE_NEAR_DELTA (0.0001)

TEST(complex, testAdditionDouble){
    math::Complex<double> comp1{1,2};
    math::Complex<double> comp2{3,1};
    math::Complex<double> result = comp1 + comp2;
    double resultReal = result.getReal();
    double resultImaginary = result.getImaginary();

    double expectedReal = 4;
    double expectedImaginary = 3;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}


TEST(complex, testMultiplicationDouble){
    math::Complex<double> comp1{2,1};
    math::Complex<double> comp2{3,1};
    math::Complex<double> result = comp1 * comp2;
    double resultReal = result.getReal();
    double resultImaginary = result.getImaginary();

    double expectedReal = 5;
    double expectedImaginary = 5;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}

TEST(complex, testDivisionDouble){
    math::Complex<double> comp1{2,1};
    math::Complex<double> comp2{3,1};
    math::Complex<double> result = comp1 / comp2;
    double resultReal = result.getReal();
    double resultImaginary = result.getImaginary();

    double expectedReal = 7.0 / 10.0;
    double expectedImaginary = -1.0/10.0;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}

TEST(complex, testSubtractionDouble){
    math::Complex<double> comp1{5,2};
    math::Complex<double> comp2{-1,3};
    math::Complex<double> result = comp1 - comp2;
    double resultReal = result.getReal();
    double resultImaginary = result.getImaginary();

    double expectedReal = 6;
    double expectedImaginary = -1;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}

TEST(complex, testIncreaseDouble){
    math::Complex<double> comp1{5,2};
    math::Complex<double> comp2{-1,3};
    comp1 += comp2;
    double resultReal = comp1.getReal();
    double resultImaginary = comp1.getImaginary();

    double expectedReal = 4;
    double expectedImaginary = 5;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}


TEST(complex, testDecreaseDouble){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{2,4};
    comp1 -= comp2;
    double resultReal = comp1.getReal();
    double resultImaginary = comp1.getImaginary();

    double expectedReal = -1;
    double expectedImaginary = -3;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}


TEST(complex, testMultiplyDouble){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{2,4};
    comp1 *= comp2;
    double resultReal = comp1.getReal();
    double resultImaginary = comp1.getImaginary();

    double expectedReal = -2;
    double expectedImaginary = 6;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}


TEST(complex, testDivisorDouble){
    math::Complex<double> comp1{2,1};
    math::Complex<double> comp2{3,1};
    comp1 /= comp2;
    double resultReal = comp1.getReal();
    double resultImaginary = comp1.getImaginary();
    
    double expectedReal = 7.0 / 10.0;
    double expectedImaginary = -1.0/10.0;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}

TEST(complex, testEqualsBothFalse){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{2,4};
    bool result = comp1 == comp2;

    bool expected = false;

    ASSERT_EQ(result, expected);
}

TEST(complex, testEqualsRealFalse){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{2,1};
    bool result = comp1 == comp2;

    bool expected = false;

    ASSERT_EQ(result, expected);
}

TEST(complex, testEqualsImaginaryFalse){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{1,2};
    bool result = comp1 == comp2;

    bool expected = false;

    ASSERT_EQ(result, expected);
}

TEST(complex, testEqualsTrue){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{1,1};
    bool result = (comp1 == comp2);

    bool expected = true;

    ASSERT_EQ(result, expected);
}


TEST(complex, testEqualsReassignment){
    math::Complex<double> comp1{1,1};
    math::Complex<double> comp2{2,2};
    comp1 = comp2;
    double resultReal = comp1.getReal();
    double resultImaginary = comp1.getImaginary();
    
    double expectedReal = 2;
    double expectedImaginary = 2;

    ASSERT_NEAR(resultReal, expectedReal, DOUBLE_NEAR_DELTA);
    ASSERT_NEAR(resultImaginary, expectedImaginary, DOUBLE_NEAR_DELTA);
}