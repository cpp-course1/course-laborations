@ECHO OFF


IF NOT EXIST "build" (
    mkdir build
    ECHO creating build directory
) ELSE (
    ECHO build directory already exists
)
cd build
cmake .. -G "MinGW Makefiles"  -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++
cmake --build .
ctest