#include <math.h>

#include "maths.h"

double functions::function(double x, double y){
    return 5*pow(x,2) + sin(x) + cosh(y); 
}