#pragma once

void print_header(double start, double end, double inc);
void print_full_line(double start, double end, double inc);
void print_start(double x);
void print_value(double val);

