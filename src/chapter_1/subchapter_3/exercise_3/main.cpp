#include <iostream>
#include <iomanip>

#include "maths.h"

#include "utilities/utils.h"

#include "main.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

const static int START_WIDTH = 20;

const static double START_VAL = 0.0;
const static double END_VAL = 1.0;
const static double INCREMENT_VAL = 0.1;

int main(){
    print_header(START_VAL, END_VAL, INCREMENT_VAL);
    print_full_line(START_VAL, END_VAL, INCREMENT_VAL);
    for (double x = START_VAL; x <= END_VAL; x+=INCREMENT_VAL){
        print_start(x);
        for (double y = START_VAL; y <= END_VAL; y+=INCREMENT_VAL){
            //print_formatted(x, y, functions::function(x, y));
            print_value(functions::function(x,y));
        }
        std::cout << std::endl;
    }
    return 0;
}



void print_start(double x){
    std::cout<< std::right;
    std::cout<< std::setw(START_WIDTH);
    std::cout<<x;
    std::cout<<"|";
}

void print_value(double value){
    std::cout<< std::right;
    std::cout<< std::setw(9);
    std::cout<<value;
    std::cout<<" ";
}

void print_header(double start, double end, double inc){
    std::cout<< std::right;
    std::cout<< std::setw(START_WIDTH);
    std::cout<<"x-labels\\y-labels ";
    std::cout<<"|";
    for (double i = start; i <= end; i+=inc){
        std::cout<<std::setw(9);
        std::cout<< i ;
        std::cout<<"|";
    }
    std::cout<<std::endl;
}
void print_full_line(double start, double end, double inc){
    std::cout<<std::right;
    std::cout<<std::setfill('-');
    std::cout<<std::setw(START_WIDTH);
    std::cout<<"";
    std::cout<<"|";
        for (double i = start; i <= end; i+=inc){
        std::cout<<std::setw(10);
        std::cout<<std::setfill('-');
        std::cout<< "" ;
    }
    std::cout<<std::setfill(' ');
    std::cout << std::endl;
}