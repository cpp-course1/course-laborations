#include <math.h>

#include "randoms.h"
#include <stdexcept>
#include <cstdlib>
#include <cmath>


/* ABSTRACT CLASS */
randomizations::Randomizer::Randomizer(){
    srand(0);
}

std::string randomizations::Randomizer::getName() const{
    return this->name;
}


// Ranomize values between 0-1
double randomizations::Randomizer::nextRandom() const{
    return (double) rand()/RAND_MAX;
}

/* GAUSSIAN */
randomizations::Gaussian::Gaussian()
:Randomizer(){
    this->name = "gaussian";
}

void randomizations::Gaussian::setStd(double std){
    this->std = std;
}

void randomizations::Gaussian::setMean(double mean){
    this->mean = mean;
}

double randomizations::Gaussian::getMean() const{
    return this->mean;
}

double randomizations::Gaussian::getStd() const{
    return this->std;
}

// IMplements the box-muller transform
// https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
double randomizations::Gaussian::generate() const{
    double u1 = this->nextRandom();
    double u2 = this->nextRandom();

    double R = sqrt(-2 * log(u1));
    double theta = 2 * M_PI * u2;

    double z1 = R * cos(theta);

    double randomValue = this->getStd() * z1 + this->getMean();
    if (isinf(randomValue)){
        return generate();
    }
    return randomValue;
}


/* POISSON */
randomizations::Poisson::Poisson()
:Randomizer(){
    this->name = "poisson";
}

void randomizations::Poisson::setLambda(double lambda){
    this->lambda = lambda;
}

double randomizations::Poisson::getLambda() const{
    return this->lambda;
}

// Generating random variables according to
// https://www.johndcook.com/blog/2010/06/14/generating-poisson-random-values/
double randomizations::Poisson::generate() const{
    double L = exp(-this->getLambda());
    int K = 0;
    double p = 1;
    double u;

    while (p > L){
        K++;
        u = this->nextRandom();
        p*=u;
    }

    return K-1;
}