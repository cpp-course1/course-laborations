#include <iostream>
#include <iomanip>
#include <new>
#include <cstdlib>

#include "randoms.h"


#include "utilities/utils.h"
#include "utilities/fileHandler.h"
#include "utilities/mathUtils.h"

#include "main.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";
const std::string& outputFileGaussian = "gaussian"; 
const std::string& outputFilePoisson = "poisson"; 

#define NUMBER_OF_RANDOMS 1000000

const double meanVal = 3.25;
const double stdVal = 2.73;
const double lambdaVal = 3.75;

int main(){
    /* Test gaussian */
    randomizations::Gaussian gaussian;
    gaussian.setMean(meanVal);
    gaussian.setStd(stdVal);

    test_randomizer(gaussian, outputFileGaussian);

    randomizations::Poisson poisson;
    poisson.setLambda(lambdaVal);

    test_randomizer(poisson, outputFilePoisson);


    return 0;
}


void test_randomizer(const randomizations::Randomizer& randomizer, const std::string& outputFileName){

    double *random;
    random = new (std::nothrow) double[NUMBER_OF_RANDOMS];

    if (random == nullptr){
        std::cout << "no allocation, i will now exit "<< std::endl;
        exit(1);
    }
    
    for (int i = 0; i< NUMBER_OF_RANDOMS; i++){
        random[i] = randomizer.generate() * 1.0;
    }

    double meanVal = mathutils::mean<double>(random, NUMBER_OF_RANDOMS);
    double stdVal = mathutils::std<double>(random, NUMBER_OF_RANDOMS);

    std::cout << randomizer.getName() << std::endl << 
    "Mean: " << meanVal << std::endl << 
    "Std: " << stdVal << std::endl <<
    "Variance: " << stdVal * stdVal << std::endl << std::endl;


    filehandler::writeListToFile<double>(outputFileName, random, NUMBER_OF_RANDOMS);

    delete[] random;
}
