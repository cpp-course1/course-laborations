#pragma once

#include <string>

namespace randomizations {
    class Randomizer {
        public:
            Randomizer();
            std::string getName() const;
            virtual double generate() const = 0;
        protected:
            double nextRandom() const;
            std::string name = "None";
    };

    
    class Gaussian: public Randomizer {
        public:
            Gaussian();
            void setMean(double mean);
            void setStd(double std);
         
            double getStd() const;
            double getMean() const;
            double generate() const;
        private: 
            double mean = 0;
            double std = 1.0;

    };

    class Poisson: public Randomizer {
        public:
            Poisson();
            double generate() const;
            void setLambda(double lambda);
            double getLambda() const;
        private: 
            double lambda = 1.0;
    };
}