#pragma once

#include "randoms.h"
#include <string>

void test_randomizer(const randomizations::Randomizer& randomizer, const std::string& outputFileName);