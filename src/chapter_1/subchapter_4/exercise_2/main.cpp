#include <iostream>

#include "maths.h"

#include "utilities/utils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";


int main(){
    double a, b, c;

    try {
        a = Utils::read_double_input("Enter the value of a: ", input_error_message);
        b = Utils::read_double_input("Enter the value of b: ", input_error_message);
        c = Utils::read_double_input("Enter the value of c: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    double result = functions::sqrt_formula(a,b,c);

    std::cout << "The largest root for the equation " << std::endl << a << "x^2+" << b << "x+" << c << "=0" << std::endl << "is when x = " << result << std::endl;

    return 0;
}
