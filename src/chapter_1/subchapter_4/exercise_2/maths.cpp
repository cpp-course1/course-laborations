#include <math.h>

#include "maths.h"
#include <stdexcept>


/*"a IS 0, UNABLE TO DIVIDE BY ZERO"

"b^2 IS LARGER THAN 4ac, UNABLE TO FINISH SQUARE"*/

double functions::sqrt_formula(double a, double b, double c){
    if (b*b < 4*a*c){
        throw std::invalid_argument("b^2 IS SMALLER THAN 4ac, UNABLE TO FINISH SQUARE");
    }
    if (a == 0){
        return -c/b;
    }

    return (-b + sqrt(b*b - 4*a*c))/(2*a);
}