#pragma once

namespace printmatrix {

    class Matrix {
        public:
            void print();
            void setColumns(int columns);
            void setRows(int rows);
        private:
            const static char char0 = '.';
            const static char char1 = '*';
            int rows = 1;
            int columns = 1;
            
            void print_row(int rowId);
            void print_cell(int rowId, int columnId);
    };
}