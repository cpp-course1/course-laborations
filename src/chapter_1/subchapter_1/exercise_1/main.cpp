#include <iostream>


#include "matrix.h"

#include "utilities/utils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    printmatrix::Matrix mat;

    try {
        int input;
        input = Utils::read_int_input("Enter the number of rows: ", input_error_message);
        mat.setRows(input);
        input = Utils::read_int_input("Enter the number of columns: ", input_error_message);
        mat.setColumns(input);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    mat.print();

    return 0;
}