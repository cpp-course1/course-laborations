#include <iostream>

#include "matrix.h"

void printmatrix::Matrix::setColumns(int columns){
    this->columns = columns;
}

void printmatrix::Matrix::setRows(int rows){
    this->rows = rows;
}

void printmatrix::Matrix::print(){
    for (int row = 0; row < rows; row++){
        print_row(row);
    }
}

void printmatrix::Matrix::print_row(int rowId){
    for (int column = 0; column < columns; column++){
        print_cell(rowId, column);
    }
    std::cout << std::endl;
}

void printmatrix::Matrix::print_cell(int rowId, int columnId){
    char character = ((rowId + columnId) % 2 == 0) ? char0 : char1;
    std::cout << character;
}
