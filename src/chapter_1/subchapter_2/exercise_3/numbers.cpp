#include "numbers.h"

#include <iostream>

int numberutils::count_digits(int digits){
    int returnVal = 0;
    do {
        returnVal++;
        digits /= 10;
    } while(digits != 0);

    return returnVal;
}
