#include <iostream>

#include "utilities/utils.h"

#include "numbers.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    int input = 0;
    
    try {
        input = Utils::read_int_input("Enter the number of rows: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    int digits = numberutils::count_digits(input);

    std::cout << "The number of digits for " << input << " are: " << digits << std::endl;

    return 0;
}