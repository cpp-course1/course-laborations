#include <iostream>

#include "utilities/utils.h"

#include "numbers.h"

const std::string input_error_message = "Wrong format on input, I will now exit!! ";

int main(){

    
    int input;

    try {
        input = Utils::read_int_input("Enter a number to show all different positive divisors: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }


    numberutils::print_all_divisors(input);

    return 0;
}