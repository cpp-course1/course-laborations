#include "numbers.h"

#include <iostream>

void numberutils::print_all_divisors(int number){
    bool isFirst = true;
    if (number < 0){
        number = -number;
    }
    for (int i = 1; i <= number; i++){
        if (is_divisor(number, i)){
            if (!isFirst){
                std::cout << ", ";
            } else {
                isFirst = false;
            }
            std::cout << i;
        }
    }
    std::cout << std::endl;
}

bool numberutils::is_divisor(int number, int divisor){
    if (divisor <= 0){
        return false;
    }

    if (number % divisor == 0){
        return true;
    }

    return false;
}