#pragma once

namespace numberutils {

    void print_all_divisors(int number);

    bool is_divisor(int number, int divisor);
}