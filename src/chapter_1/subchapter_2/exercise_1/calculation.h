#pragma once

namespace calculations {

    int first();
    int second(int a);

}