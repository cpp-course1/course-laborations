#pragma once

namespace functions {
    double geometric_progression(double a, double r, int n);
}