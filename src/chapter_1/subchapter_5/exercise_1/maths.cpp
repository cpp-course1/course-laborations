#include <math.h>

#include "maths.h"
#include <stdexcept>



double functions::geometric_progression(double a, double r, int n){
    if (n < 0){
        throw std::invalid_argument("n IS SMALLER THAN 0, UNABLE TO FINISH SQUARE");
    }

    return a * pow(r, n);
}
