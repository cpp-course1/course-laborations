#include <iostream>
#include <iomanip>

#include "maths.h"

#include "utilities/utils.h"

#include "main.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

const int COLUMN_WITH = 10;
const int SMALL_COLUMN_WITH = 5;

int main(){
    double a = 0, r = 0;
    int n = 0;

    try {
        a = Utils::read_double_input("Enter the value of a: ", input_error_message);
        r = Utils::read_double_input("Enter the value of r: ", input_error_message);
        n = Utils::read_int_input("Enter the number of progressions n: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    double result = functions::geometric_progression(a, r, n);

    print_header();
    print_full_line();
    for (double x = 0; x <= n; x++){
        print_formatted(a, r, x, functions::geometric_progression(a,r,x));
    }
    return 0;
/*
    std::cout << "The largest root for the equation " << std::endl << a << "x^2+" << b << "x+" << c << "=0" << std::endl << "is when x = " << result << std::endl;*/

    return 0;
}


void print_header(){
    std::cout<<std::right;
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<<"x " <<"|";
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<<"r " << "|";
    std::cout<<std::setw(SMALL_COLUMN_WITH);
    std::cout<<"n " << "|";
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<<"progress " << std::endl;
}
void print_full_line(){
    std::cout<<std::right;
    std::cout<<std::setfill('-');
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<< "" << "|";
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<< "" << "|";
    std::cout<<std::setw(SMALL_COLUMN_WITH);
    std::cout<< "" << "|";
    std::cout<<std::setw(COLUMN_WITH);
    std::cout<< "" << std::endl;
    std::cout<<std::setfill(' ');
}

void print_formatted(double a, double r, int n, double prog){
    std::cout<<std::right;
    std::cout<<std::setw(COLUMN_WITH - 1);
    std::cout<< a << " " <<"|";
    std::cout<<std::setw(COLUMN_WITH - 1);
    std::cout<< r << " " <<"|";
    std::cout<<std::setw(SMALL_COLUMN_WITH - 1);
    std::cout<< n << " " <<"|";
    std::cout<<std::setw(COLUMN_WITH - 1);
    std::cout<< prog << " " <<std::endl;
}