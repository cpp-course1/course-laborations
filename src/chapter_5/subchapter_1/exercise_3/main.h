#pragma once

#include <vector>

std::vector<std::vector<int>> getRandomVector(int width, int height);

void printMatrix(std::vector<std::vector<int>>  matrix);

int sumMatrixRow(std::vector<int> row);