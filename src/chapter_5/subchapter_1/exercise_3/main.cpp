#include <iostream>
#include <stdlib.h>
#include <time.h> 

#include "utilities/utils.h"

#include "main.h"
#include <algorithm>
#include <iomanip>

const std::string input_error_message = "Wrong format on input, I will not exit!! ";
const uint MAX_RANDOM_VAL = 100;

int main(){
    int width, height;
    srand(time(NULL));

    

    try {
        width = Utils::read_int_input("Enter the number of rows: ", input_error_message);
        height = Utils::read_int_input("Enter the number of columns: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    std::cout << "Creating matrix with size (" << width << ", " << height << ")" << std::endl; 

    std::vector<std::vector<int>> matrix = getRandomVector(width, height);

    printMatrix(matrix);

    std::cout << std::endl << std::endl;

    sort(matrix.begin(), matrix.end(), 
        [](std::vector<int> vec1, std::vector<int> vec2){
            return sumMatrixRow(vec1)>sumMatrixRow(vec2);
        });

    std::cout << "Sorted matrix (by row sum)" << std::endl;
    printMatrix(matrix);
    return 0;
}

int sumMatrixRow(std::vector<int> row){
    int sum{0};
    for (auto e: row){
        sum += e;
    }
    return sum;
}


std::vector<std::vector<int>> getRandomVector(int width, int height){
    std::vector<std::vector<int>> matrix(height);
    for (int row = 0; row < height; row++){
        std::vector<int> matrixRow(width); // allocate all memory at once
        for (int column = 0; column < width; column++){
            matrixRow[column] = rand() % MAX_RANDOM_VAL;
        }
        matrix[row] = matrixRow;
    }
    return matrix;
}


void printMatrix(std::vector<std::vector<int>>  matrix){
    for (auto row : matrix){
        for (auto column : row){
            std::cout<<std::right;
            std::cout<<std::setw(3);
            std::cout << column << " "; 
        }
        std::cout << "\tsum: " << sumMatrixRow(row) << std::endl;
    }
}