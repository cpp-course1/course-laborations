#include "utilities/fileHandler.h"

#include "main.h"

const std::string fileName = "file_for_5_2_3.txt";

int main(){

    /* From Ex 3 1 1*/
    std::cout << "Output from Ex 3:1_1 " << std::endl;
    filehandler::printStatisticsOfFile(fileName);
    std::cout << std::endl << std::endl;

    std::map<std::string,int> wordCounts = filehandler::getWordsCounter(fileName);

    printMapContainerContent(wordCounts);

    return 0;
}


void printMapContainerContent(std::map<std::string,int> mapContainer){
    for (auto e: mapContainer){
        std::cout << e.first << "\t" << e.second << std::endl;
    }
}
