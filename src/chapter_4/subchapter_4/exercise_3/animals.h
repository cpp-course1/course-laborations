class Animal{
    public:
        virtual void Name() {}
};

class Cat :  public Animal{
    public:
        void Name();
};


class Dog :  public Animal{
    public:
        void Name();
};


class Bird :  public Animal{
    public:
        void Name();
};