#include "animals.h"
#include <list>
#include <stdlib.h>

const static int NUMBER_ANIMALS = 15;

int main(){
    std::list<Animal*> animalList;

    for (int i = 0; i < NUMBER_ANIMALS; i++){
        Animal* animal;
        switch (rand() % 3)
        {
        case 0:
            animal = new Cat();
            break;
        case 1:
            animal = new Dog();
            break;
        case 2:
        default:
            animal = new Bird();
            break;
        }
        animalList.push_back(animal);
    }


    for (Animal* animal : animalList){
        animal->Name();
    }

    for (Animal* animal : animalList){
        delete animal;
    }

    return 0;
}