#include <iostream>

class Base {
    protected: // 2. Changed to protected from private so that derived classes knows these exist and can access them
        int field1;
        int field2;
    public: 
        Base() : field1(5), field2(10) {};
};

class Derived : public Base {
    public: 
        int Product() {
            return field1 * field2; // 1. Derived do not know anything about field1 and field2 since they are private in Base 
        }
};

int main(int argc, char* argv[]){
    Derived obj;
    std::cout << obj.Product() << std::endl;
    return 0;
}