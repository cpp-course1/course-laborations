#include<iostream>

class Base{
    public:
        Base(){
            std::cout << "Base class constructor is called" << std::endl;
        }
        Base(int i){
            std::cout << "Base class constructor is called with: " << i << std::endl;
        }
        Base(double i){
            std::cout << "Base class constructor is called with double: " << i << std::endl;
        }
};

class Derived : public Base{
    public:
        Derived(){ // same as-> Derived() : Base() {
            std::cout << "Derived class constructor is called" << std::endl;
        }
        Derived(int i) : Base(i){
            std::cout << "Derived class constructor is called with: " << i << std::endl;
        }
        Derived(double i){ // same as-> Derived(double i) : Base() { 
            std::cout << "Derived class constructor is called with double: " << i << std::endl;
        }
};

int main(int argc, char* argv[]){

    Derived obj;
    Derived obj2{4};
    Derived obj3{4.0};
    return 0;
}