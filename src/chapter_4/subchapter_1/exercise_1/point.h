#pragma once

namespace point{

    class Point {
        private:
            double x, y, z;
        public:
            Point();
            Point(const double& x, const double& y, const double& z);
            void Print();
            double Radius();
            double Theta();
            double Phi();
            void PrintSpherical();
    };




}