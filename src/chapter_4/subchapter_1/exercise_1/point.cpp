#include "point.h"
#include <cmath>
#include <iomanip>
#include <iostream>

#define COLUMN_WIDTH (10)
#define DOUBLE_PRECISION (7)

namespace point {
    Point::Point(){
        this->x = 0;
        this->y = 0;
        this->z = 0;
    }
    
    Point::Point(const double& x,
        const double& y,
        const double& z): x{x}, y{y}, z{z}{
    }


    void Point::Print(){
        std::cout<<std::right;
        std::cout<<"x:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << this->x << std::setw(COLUMN_WIDTH) << " y:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << this->y << std::setw(COLUMN_WIDTH) << " z:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << this->z << std::endl<< std::left;
    }

    double Point::Radius(){
        return sqrt(x*x + y*y + z*z);
    }

    // https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations#To_spherical_coordinates
    double Point::Theta(){
        double numerator = sqrt(x*x + y*y);
        return atan2(numerator, z);
    }


    // https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations#To_spherical_coordinates
    double Point::Phi(){
        return atan2(y, x);
    }

    void Point::PrintSpherical(){
        double radius = this->Radius();
        double theta = this->Theta();
        double phi = this->Phi();

        std::cout<<std::right;
        std::cout<<"r:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << radius << std::setw(COLUMN_WIDTH) << " theta:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << theta << std::setw(COLUMN_WIDTH) << " phi:";
        std::cout << std::setw(COLUMN_WIDTH) << std::setprecision(DOUBLE_PRECISION);
        std::cout << phi << std::endl << std::left;
    }

}