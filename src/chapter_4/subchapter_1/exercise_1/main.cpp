#include "main.h"
#include "point.h"

#include <iostream>

int main(){

    test_point(0,0,0);

    test_point(1,0,0);

    test_point(1,1,1);

    test_point(3, 0, 0);

    return 0;
}


void test_point(double x, double y, double z){
    point::Point point(x, y, z);

    point.Print();
    
    point.PrintSpherical();

    std::cout << std::endl;
}