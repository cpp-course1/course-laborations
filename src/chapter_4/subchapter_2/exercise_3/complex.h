#pragma once

namespace math{
    enum Print_Format {
        general,
        fixed,
        scientific
    };

    template <typename T>
    class Complex {
        private:
            T real;
            T imaginary;
            static Print_Format print_format;
            void PrintValue(const T& value) const;
        public:
            Complex();
            Complex(const T& real, const T& imaginary);
            static void SetPrintFormat(const Print_Format& format);
            void Print() const;
            T getReal() const;
            T getImaginary() const;
            
            // Operators
            Complex operator+(const Complex& other);
            Complex operator-(const Complex& other);
            Complex operator/(const Complex& other);
            Complex operator*(const Complex& other);
            Complex operator+=(const Complex& other);
            Complex operator-=(const Complex& other);
            Complex operator*=(const Complex& other);
            Complex operator/=(const Complex& other);
            bool operator==(const Complex& other) const;
            Complex& operator=(const Complex& other);
            // Unable to overload
            //Complex operator=(const Complex& other);
    };

}