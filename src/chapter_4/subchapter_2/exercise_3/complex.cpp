#include "complex.h"
#include <iostream>

#define COLUMN_WIDTH (10)
#define DOUBLE_PRECISION (7)

namespace math{
    template <typename T>
    Print_Format Complex<T>::print_format = general;

    template<> 
    Print_Format Complex<double>::print_format = scientific;
    
    template<> 
    Print_Format Complex<float>::print_format = fixed;

    template <typename T>
    Complex<T>::Complex(){}

    template <typename T>
    Complex<T>::Complex(const T& real, const T& imaginary): real{real}, imaginary{imaginary}{}

    template <typename T>
    void Complex<T>::SetPrintFormat(const Print_Format& format){
        Complex<T>::print_format = format;
    }

    template <typename T>
    void Complex<T>::PrintValue(const T& value) const{
        switch (Complex<T>::print_format){
            case (general):
                std::cout << value;
                break;
            case (fixed):
                std::cout << std::fixed << value ;
                break;
            case (scientific):
                std::cout << std::scientific << value ;
                break;
            default:
                std::cout << value;
                break;
        }
    }

    template <typename T>
    void Complex<T>::Print() const{
        std::cout << "Real: ";  
        PrintValue(this->getReal());
        std::cout << "\tImaginary: "; 
        PrintValue(this->getImaginary());
        std::cout << std::endl; 
    }

    template <typename T>
    T Complex<T>::getReal() const{
        return this->real;
    }

    template <typename T>
    T Complex<T>::getImaginary() const{
        return this->imaginary;
    }
    
    template <typename T>
    Complex<T> Complex<T>::operator+(const Complex<T>& other){
        double outReal = this->getReal() + other.getReal();
        double outImaginary = this->getImaginary() + other.getImaginary();

        return Complex<T>(outReal, outImaginary);
    }

    template <typename T>
    Complex<T> Complex<T>::operator*(const Complex<T>& other){
        T r1 = this->getReal();
        T r2 = other.getReal();

        T i1 = this->getImaginary();
        T i2 = other.getImaginary();

        T outReal = r1*r2 - i1*i2;
        T outImaginary = r1*i2 + r2*i1;

        return Complex<T>(outReal, outImaginary);
    }

    template <typename T>
    Complex<T> Complex<T>::operator-(const Complex<T>& other){
        double outReal = this->getReal() - other.getReal();
        double outImaginary = this->getImaginary() - other.getImaginary();

        return Complex<T>(outReal, outImaginary);
    }

    template <typename T>
    Complex<T> Complex<T>::operator/(const Complex<T>& other){
        T r1 = this->getReal();
        T r2 = other.getReal();

        T i1 = this->getImaginary();
        T i2 = other.getImaginary();

        T denominator = r2*r2 + i2*i2;

        T outReal = (r1*r2 + i1*i2) / denominator;
        T outImaginary = (r1*i2 - r2*i1) / denominator;

        return Complex<T>(outReal, outImaginary);
    }

    template <typename T>
    Complex<T>& Complex<T>::operator=(const Complex<T>& other){
        this->real = other.getReal();
        this->imaginary = other.getImaginary();
        return *this;
    }

    template <typename T>
    Complex<T> Complex<T>::operator+=(const Complex<T>& other){
        this->real += other.getReal();
        this->imaginary += other.getImaginary();

        return *this;
    }

    template <typename T>
    Complex<T> Complex<T>::operator-=(const Complex<T>& other){
        this->real -= other.getReal();
        this->imaginary -= other.getImaginary();

        return *this;
    }

    template <typename T>
    Complex<T> Complex<T>::operator*=(const Complex<T>& other){
        T r1 = this->getReal();
        T r2 = other.getReal();

        T i1 = this->getImaginary();
        T i2 = other.getImaginary();

        T outReal = r1*r2 - i1*i2;
        T outImaginary = r1*i2 + r2*i1;

        this->real = outReal;
        this->imaginary = outImaginary;

        return *this;
    }

    template <typename T>
    Complex<T> Complex<T>::operator/=(const Complex<T>& other){
        T r1 = this->getReal();
        T r2 = other.getReal();

        T i1 = this->getImaginary();
        T i2 = other.getImaginary();

        T denominator = r2*r2 + i2*i2;

        T outReal = (r1*r2 + i1*i2) / denominator;
        T outImaginary = (r1*i2 - r2*i1) / denominator;

        this->real = outReal;
        this->imaginary = outImaginary;

        return *this;
    }

    template <typename T>
    bool Complex<T>::operator==(const Complex<T>& other) const{
        T otherReal = other.getReal();
        if (this->getReal() != otherReal ){
            return false;
        }
        T otherImaginary = other.getImaginary();
        if (this->getImaginary() != otherImaginary){
            return false;
        }

        return true;
    }

    template class Complex<double>;
    template class Complex<float>;
    template class Complex<int>;
    template class Complex<long>;
}