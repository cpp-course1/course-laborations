#include "main.h"
#include "complex.h"

#include <iostream>

int main(){
    math::Complex<int>(1,2).Print();
    math::Complex<double>(1,2).Print();
    math::Complex<float>(1,2).Print();

    std::cout << "!!!!!!!! Testing complex double !!!!!!!!!" << std::endl;
    test_complex(math::Complex<double>(2.5,1));

    std::cout << "!!!!!!!! Testing complex float !!!!!!!!!" << std::endl;
    test_complex(math::Complex<float>(-10.3f,20));

    std::cout << "!!!!!!!! Testing complex int !!!!!!!!!" << std::endl;
    test_complex(math::Complex<int>(4,5));
    
    std::cout << "!!!!!!!! Testing complex long !!!!!!!!!" << std::endl;
    test_complex(math::Complex<long>(3,1));


    std::cout << "******** Testing different print settings ********" << std::endl << std::endl;

    std::cout << " *** testing general ****" << std::endl;
    math::Complex<double>::SetPrintFormat(math::Print_Format::general);
    test_complex(math::Complex<double>(2.5,1));

    
    std::cout << " *** testing fixed ****" << std::endl;
    math::Complex<double>::SetPrintFormat(math::Print_Format::fixed);
    test_complex(math::Complex<double>(2.5,1));

    std::cout << " *** testing scientific ****" << std::endl;
    math::Complex<double>::SetPrintFormat(math::Print_Format::scientific);
    test_complex(math::Complex<double>(2.5,1));

    return 0;
}

template<typename T> 
void test_complex(math::Complex<T> complex){
    std::cout << std::endl << "Complex number c " << std::endl;
    complex.Print();
    complex = complex + complex;
    std::cout << std::endl << "c+c=c1" << std::endl;
    complex.Print();
    complex = complex - math::Complex<T>(3,2);
    std::cout << std::endl << "c1-3-2i=c2" << std::endl;
    complex.Print();
    complex = complex / math::Complex<T>(2,3);
    std::cout << std::endl << "c2/(2+3i)=c3" << std::endl;
    complex.Print();
    complex = complex * math::Complex<T>(4,-2);
    std::cout << std::endl << "c3*(4-2i)=c4" << std::endl;
    complex.Print();
    complex += complex;
    std::cout << std::endl << "c4+c4=c5" << std::endl;
    complex.Print();
    complex -= math::Complex<T>(4,-2);;
    std::cout << std::endl << "c5-(4-2i)=c6" << std::endl;
    complex.Print();
    complex *= math::Complex<T>(4,-2);;
    std::cout << std::endl << "c6*(4-2i)=c7" << std::endl;
    complex.Print();
    complex /= complex;
    std::cout << std::endl << "c7/c7=c8" << std::endl;
    complex.Print();
    bool equal = (complex == math::Complex<T>(1,0));
    std::cout << std::endl << "is c8==(1+0i)?" << std::endl;
    std::cout << std::endl << (equal ? "true" : "false") << std::endl;
    complex = math::Complex<T>(-1,-1);
    std::cout << std::endl << "c9=-1-1i" << std::endl;
    complex.Print();

    std::cout << std::endl<< std::endl;
}