#include <iostream>

#include "utilities/utils.h"

int Utils::read_int_input(const std::string &input_string,
    const std::string &error_string){
        std::cin.exceptions(std::istream::failbit);

        int outVal = 0;
        try{
            std::cout << input_string;
            std::cin >> outVal;
        } 
        catch (const std::exception& e){
            throw std::invalid_argument(error_string);
        }

        return outVal;
    }   



double Utils::read_double_input(const std::string &input_string,
    const std::string &error_string){
        std::cin.exceptions(std::istream::failbit);

        double outVal = 0;
        try{
            std::cout << input_string;
            std::cin >> outVal;
        } 
        catch (const std::exception& e){
            throw std::invalid_argument(error_string);
        }

        return outVal;
    }   


std::string Utils::read_string_input(const std::string &input_string, const std::string &error_string){

        std::cin.exceptions(std::istream::failbit);
        
        std::string outString{""};
        try{
            std::cout << input_string;
            std::cin >> outString;
        } 
        catch (const std::exception& e){
            throw std::invalid_argument(error_string);
        }

        return outString;
}