#include "utilities/fileHandler.h"
#include "config/config.h"
#include <iostream>
#include <sstream>

#include <fstream>

namespace filehandler{
    
    template<typename T>
    void writeListToFile(std::string filename, T list[], int length){
        std::fstream fileOut{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileOut.open(absoluteFilename, std::ios::trunc | std::ios::out);

        for (int i = 0; i < length; i++){
            fileOut << list[i] << std::endl;
        }
        fileOut.close();
    }

    void writeListToFile(std::string filename, double* list, int length){
        std::fstream fileOut{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileOut.open(absoluteFilename, std::ios::trunc | std::ios::out);
        for (int i = 0; i < length; i++){
            fileOut << list[i] << std::endl;
        }
        fileOut.close();
    }

    template void writeListToFile<double>(std::string filename, double* list, int length);
    template void writeListToFile<int>(std::string filename, int* list, int length);
    template void writeListToFile<float>(std::string filename, float* list, int length);
    template void writeListToFile<long>(std::string filename, long* list, int length);


    void printStatisticsOfFile(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);

        int number_chars = getNumberOfCharacters(filename);

        int number_numbers = getNumberOfNumbers(filename);

        int number_words = getNumberOfWords(filename);
        int number_lines = getNumberOfLines(filename);


        std::cout << "Number of characters: " << number_chars << std::endl;
        std::cout << "Number of numbers: " << number_numbers << std::endl;
        std::cout << "Number of words: " << number_words << std::endl;
        std::cout << "Number of lines: " << number_lines << std::endl;
    }

    std::string getAbsolutePath(std::string filename){
        return config::dataInputLocation + "/" + filename;
    }

    int getNumberOfCharacters(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileIn.open(absoluteFilename, std::ios::in);
        int number_chars{0};
        char dummy;
        while(fileIn.get(dummy)){
            number_chars++;
        }

        fileIn.close();
        return number_chars;
    }    

    int getNumberOfNumbers(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileIn.open(absoluteFilename, std::ios::in);

        int number_numbers{0};
        std::string nextWord{" "};
        while(fileIn >> nextWord){
            if (isANumber(nextWord)){
                number_numbers++;
            }
        }

        return number_numbers;
    }

    bool isANumber(std::string str){
        int dummyNumber{0};
        std::stringstream converterStream{};
        // put the word to the stringstream
        converterStream << str;
        // try to take what is in the stringstream to an integer
        converterStream >> dummyNumber;
        // if the stringstream coulnt create the integer the fail flag will be set
        return !converterStream.fail();
    }

    int getNumberOfWords(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileIn.open(absoluteFilename, std::ios::in);

        
        int number_words{0};
        std::string nextWord{""};
        while(fileIn >> nextWord){
            number_words++;
        }


        fileIn.close();

        return number_words;
    }

    int getNumberOfLines(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileIn.open(absoluteFilename, std::ios::in);

        
        int number_lines{0};
        std::string nextLine{""};
        while(std::getline(fileIn,nextLine)){
            number_lines++;
        }


        fileIn.close();

        return number_lines;
    }

    std::map<std::string, int> getWordsCounter(std::string filename){
        std::fstream fileIn{};
        std::string absoluteFilename = getAbsolutePath(filename);
        fileIn.open(absoluteFilename, std::ios::in);
        std::map<std::string, int> wordCounts;

        std::string nextWord;
        while(fileIn >> nextWord){
            wordCounts[nextWord] += 1;
        }
        return wordCounts;
    }
}

