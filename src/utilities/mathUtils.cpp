#include "utilities/mathUtils.h"

#include "math.h"

#include <iostream>

#define MEAN_MAX_SIZE 1000

namespace mathutils{
    
    template<typename T>
    double mean (T list[], int length){
        if (length == 0){
            return 0;
        }
        if (length > MEAN_MAX_SIZE){
            return meanRecursive(list, 0, length);
        }

        T sum = 0;
        
        for (int i = 0; i< length; i++){
            sum += list[i];
        }

        return (double) sum / length;
    }

    
    template<typename T>
    double meanRecursive(T list[], int start, int end){
        if (end - start > MEAN_MAX_SIZE){
            int middle = start + (end - start) / 2;
            double mean1 = meanRecursive(list, start, middle);
            double mean2 = meanRecursive(list, middle, end);
            return (mean1 + mean2) / 2;
        }

        return mean(&list[start], end - start);
    }

    
    
    template<typename T>
    double std(T list[], int length){
        if (length == 0){
            return 0;
        }

        double mean = mathutils::mean<T>(list, length);
        

        double sum = 0;
        for (int i = 0; i< length; i++){
            sum += pow(list[i] - mean, 2);
        }
        return sqrt(sum / length);
    }

    

    template<typename T>
    T max(T list[], int length){
        if (length == 0){
            throw std::invalid_argument("Unable to find maximum element of list of length 0");
        }
        T currentVal = list[0];
        T max = currentVal;
        for (int i = 1; i < length; i++){
            currentVal = list[i];
            if (max < currentVal){
                max = currentVal;
            }
        }
        return max;
    }

    template<typename T>
    T min(T list[], int length){
        if (length == 0){
            throw std::invalid_argument("Unable to find minimum element of list of length 0");
        }
        T currentVal = list[0];
        T min = currentVal;
        for (int i = 1; i < length; i++){
            currentVal = list[i];
            if (min > currentVal){
                min = currentVal;
            }
        }
        return min;
    }




    // I think we only need to create templates for std since it uses the mean function with the same signerature
    template double std<double>(double* list, int length);
    template double std<int>(int* list, int length);
    template double std<float>(float* list, int length);
    template double std<long>(long* list, int length);
    
    template double max<double>(double* list, int length);
    template int max<int>(int* list, int length);
    template float max<float>(float* list, int length);
    template long max<long>(long* list, int length);

    template double min<double>(double* list, int length);
    template int min<int>(int* list, int length);
    template float min<float>(float* list, int length);
    template long min<long>(long* list, int length);
    
}