#pragma once

namespace function {
    template<typename T>
    double* derivateList(double (*f)(double), T* valueList, int length);

    template<typename T>
    double derivate(double (*f)(double), T value);

}