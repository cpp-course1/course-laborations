#pragma once

#include <string>

void test_function(double (*function)(double), std::string functionName, double* values, int length);