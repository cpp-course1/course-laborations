#include "main.h"
#include <iostream>
#include <iomanip>
#include <cmath>

#include "function.h"

#define TABLE_WIDTH 10
#define NUMBER_VALUES 6

int main(){

    double values[NUMBER_VALUES]{1.0, 2.0, 3.0, 4.0, 5.0, 6.0};

    test_function([](auto x)->auto{return x*x;}, "Quadratic, f(x)=x^2", values, NUMBER_VALUES);

    test_function([](auto x)->auto{return exp(x);}, "Exponential, f(x)=e^x", values, NUMBER_VALUES);

    test_function([](auto x)->auto{return sin(x);}, "Sinusoidal, f(x)=sin(x)", values, NUMBER_VALUES);

    test_function([](auto x)->auto{return cos(x);}, "Quadratic, f(x)=cos(x)", values, NUMBER_VALUES);

    test_function([](auto x)->auto{return log(x);}, "Quadratic, f(x)=ln(x)", values, NUMBER_VALUES);

    return 0;
}

void test_function(double (*function)(double), std::string functionName, double* values, int length){

    std::cout << "Testing derivatives for function " << functionName << std::endl;
    std::cout << std::right;
    std::cout << std::setw(TABLE_WIDTH);
    std::cout << "x" << "|";
    std::cout << std::setw(TABLE_WIDTH);
    std::cout << "x'" << std::endl;
    std::cout << std::setw(TABLE_WIDTH);
    std::cout << std::setfill('-');
    std::cout << "" << "-";
    std::cout << std::setw(TABLE_WIDTH);
    std::cout << std::setfill('-');
    std::cout << "" << std::endl;
    std::cout << std::setfill(' ');

    double* derivatives = function::derivateList<double>(function, values, length);

    for (int d = 0; d < length; d++){
        std::cout << std::setw(TABLE_WIDTH) << std::setprecision(7);
        std::cout << values[d] << "|";
        std::cout << std::setw(TABLE_WIDTH) << std::setprecision(7);
        std::cout << derivatives[d] << std::endl;
    }


    delete[] derivatives;
}