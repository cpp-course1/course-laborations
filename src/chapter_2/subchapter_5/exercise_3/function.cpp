#include "function.h"
#include <string>

#define derivateDelta       (0.00001)
#define doubleDerivateDelta (derivateDelta*2)
    

namespace function{
    template<typename T>
    double* derivateList(double (*f)(double), T* valueList, int length){
        double* derivates = new double[length];
        for (int i = 0; i < length; i++){
            derivates[i] = derivate<T>(f, valueList[i]);
        }

        return derivates;
    }

    template<typename T>
    double derivate(double (*f)(double), T value){
        T lowerBound = value - derivateDelta;
        T upperBound = value + derivateDelta;

        double functionValueLower = f(lowerBound);
        double functionValueUpper = f(upperBound); 

        double derivative = (functionValueUpper - functionValueLower) /doubleDerivateDelta;

        return derivative;
    }

    template double derivate<double>(double (*f)(double), double value);
    template double* derivateList<double>(double (*f)(double), double* valueList, int lenght);

    template double derivate<int>(double (*f)(double), int value);
    template double* derivateList<int>(double (*f)(double), int* valueList, int lenght);
}
