# Set binary test file
set(EXERCISE exercise-2_1_1)
set(BINARY ${EXERCISE})

# find all source and include files from this folder, its recursive
file(GLOB_RECURSE TEST_SOURCES LIST_DIRECTORIES false *.h *.cpp)

# set the files to a source  variable
set(SOURCES ${TEST_SOURCES})

add_executable(${BINARY}_run ${SOURCES})

add_library(${BINARY}_lib STATIC ${SOURCES})

# Which extra libraries do we want??
target_link_libraries (${BINARY}_run LINK_PUBLIC Utilities)