#include <iostream>
#include <iomanip>


#include "main.h"
#include "utilities/utils.h"
#include "utilities/mathUtils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    int arr[]{1,2,3,4,5,6};
    
    print_info<int>(arr, 6);


    return 0;
}


template<typename T>
void print_info(T list[], int length){
    
    double mean = mathutils::mean<T>(list, 6);
    double std = mathutils::std<T>(list, 6);
    T min = mathutils::min<T>(list, 6);
    T max = mathutils::max<T>(list, 6);

    std::cout << "New List: ";
    std::cout << "[" << list[0];
    for (int i = 1; i < length; i++){
        std::cout << ", " << list[i];
    }
    std::cout <<"]" << std::endl << std::endl;

    print_header();
    print_delimiter();

    print_formatted_value<double>(mean, "Mean");
    print_formatted_value<double>(std, "Std");
    print_formatted_value<T>(min, "Min");
    print_formatted_value<T>(max, "Max");

    std::cout << std::endl << std::endl;
}

void print_header(){
    std::cout<<std::right;
    std::cout<<std::setw(10);
    std::cout << "Function ";
    std::cout<<"|";
    std::cout<<std::setw(10);
    std::cout<<"value "<<std::endl;
}

void print_delimiter(){
    
    std::cout<<std::right;
    std::cout<<std::setw(10);
    std::cout<<std::setfill('-');
    std::cout << "";
    std::cout << "-";
    std::cout<<std::setw(10);
    std::cout<<std::setfill('-');
    std::cout<<""<<std::endl;
    std::cout<<std::setfill(' ');
}

template <typename T>
void print_formatted_value(const T& value, const std::string& typeName){
    std::cout<< std::right;
    std::cout<< std::setw(9);
    std::cout<< typeName << " ";
    std::cout<< "|";
    std::cout<< std::setw(9);
    std::cout<< value<<std::endl;
}
