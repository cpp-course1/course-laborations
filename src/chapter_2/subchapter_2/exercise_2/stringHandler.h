#pragma once

#include <string>

namespace stringhandler {
    bool const isPalindrome(const std::string& palindrome);
}