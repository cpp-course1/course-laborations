#include <iostream>
#include <iomanip>
#include "stringHandler.h"

#include <string>


#include "main.h"
#include "utilities/utils.h"
#include "utilities/mathUtils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    std::string input;
    try{
        input = Utils::read_string_input("Enter a string to check if it is a palindrome: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    bool isPalindrome = stringhandler::isPalindrome(input);

    if (isPalindrome){
        std::cout << input << " is a palindrome" << std::endl;
    } else {
        std::cout << input << " is NOT a palindrome" << std::endl;
    }

    return 0;
}
