#include "stringHandler.h"
#include <iostream>

bool const stringhandler::isPalindrome(const std::string& palindrome){
    
    for (std::string::const_iterator iteratorLow = palindrome.begin(), // init
        iteratorHigh = palindrome.end() - 1; // init
        iteratorLow < iteratorHigh; // condition
        iteratorHigh--, iteratorLow++ ){ // loop

        if (*iteratorLow != *iteratorHigh){
            return false;
        }
    }
    return true;
}