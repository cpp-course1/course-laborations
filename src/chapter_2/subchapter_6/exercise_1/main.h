#pragma once


int* createArray(const int& length);
void deleteArray(int* array);
int getLengthOfArray();
void fillArrayWithRandom(int* array, const int& length);
void printArray(const int* array, const int& length);