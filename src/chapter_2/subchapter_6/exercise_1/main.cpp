#include "main.h"
#include <iostream>
#include <iomanip>
#include <string>
#include <stdlib.h>

#include "utilities/utils.h"

const std::string& input_error_message = "Wrong format on input, I will now exit!! ";

const std::string negative_input_error_message = "The input need to be larger than 0, I will now exit!! ";

int main(){
    srand(0);

    int arrayLength = getLengthOfArray();

    int array[arrayLength];

    fillArrayWithRandom(array, arrayLength);

    printArray(array, arrayLength);


    return 0;
}

int getLengthOfArray(){
    int arrayLength{0};

    try{
        arrayLength = Utils::read_int_input("Enter the length of the array: ", input_error_message);
        if (arrayLength < 0){
            throw std::invalid_argument(negative_input_error_message);
        }
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return arrayLength;
}

int* createArray(const int& length){
    int* array = new int[length];

    return array;
}

void printArray(const int* array, const int& length){    
    for (int i = 0; i < length; i++){
        std::cout << array[i] << " " << std::endl;
    }
    std::cout << std::endl;
}

void fillArrayWithRandom(int* array, const int& length){
    for (int i = 0; i< length; i++){
        array[i] = rand();
    }
}

void deleteArray(int* array){
    delete[] array;
}
 
