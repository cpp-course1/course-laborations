#pragma once

#include <string>

namespace matrix {

    int* createMatrix(int width, int height);
    void printMatrix(int* matrix, int width, int height);
    void cleanupMatrix(int* matrix);

    int** doublePointerMatrix(int* matrix, int width, int height);
    void printDoublePointerMatrix(int** matrix, int widht, int height);
    void cleanupDoublePointerMatrix(int** matrix);
}