#include "matrix.h"
#include <iostream>
#include <iomanip>

#include <cstdlib>

#define PRINT_WIDTH 11
    

int* matrix::createMatrix(int width, int height){
    srand(0);
    int* matrix = new int[height*width];

    for (int e = 0; e < width* height; e++){
        matrix[e] = rand();
    }
    
    return matrix;
}

void matrix::printMatrix(int* matrix, int width, int height){
    
    for (int h = 0; h < height; h++){
        for (int w = 0 ; w < width; w++){
            std::cout<<std::right;
            std::cout<<std::setw(PRINT_WIDTH);
            std::cout << matrix[h *width + w ] << " ";
        }
        std::cout << std::endl;
    }
}

void matrix::cleanupMatrix(int* matrix){
    delete[] matrix;
    matrix = 0;
}



int** matrix::doublePointerMatrix(int* matrix, int width, int height){
    int** newMatrix = new int*[height];
    for (int h = 0; h < height; h++){
        newMatrix[h] = &matrix[width * h];
    }

    return newMatrix;
}

void matrix::printDoublePointerMatrix(int** matrix, int width, int height){
    for (int h = 0; h < height; h++){
        for (int w = 0; w < width; w++){
            std::cout << std::right;
            std::cout << std::setw(PRINT_WIDTH);
            std::cout << matrix[h][w] << " ";
        }
        std::cout << std::endl;
    }
}
void matrix::cleanupDoublePointerMatrix(int** matrix){
    delete[] matrix;
    matrix = 0;
}