#include <iostream>
#include <iomanip>
#include "matrix.h"

#include <string>

#include "utilities/utils.h"
#include "utilities/mathUtils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    int width, height;
    try{
        width = Utils::read_int_input("Enter a nubmer for the width of the matrix: ", input_error_message);
        height = Utils::read_int_input("Enter a nubmer for the height of the matrix: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }
    
    int* matrix = matrix::createMatrix(width, height);

    
    std::cout << std::endl << "Print pointer matrix" << std::endl;
    matrix::printMatrix(matrix, width, height);

    int** doubleMatrix = matrix::doublePointerMatrix(matrix, width, height);
    std::cout << std::endl << "Print pointer row matrix" << std::endl;
    matrix::printDoublePointerMatrix(doubleMatrix,  width,  height);
    matrix::cleanupDoublePointerMatrix(doubleMatrix);

    matrix::cleanupMatrix(matrix);




    return 0;
}
