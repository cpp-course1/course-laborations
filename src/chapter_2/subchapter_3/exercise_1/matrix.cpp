#include "matrix.h"
#include <iostream>
#include <iomanip>


int** matrix::createMatrix(int width, int height){
    int** matrix = new int*[height];

    for (int h = 0; h < height; h++){
        matrix[h] = new int[width];
        for (int w = 0; w < width; w++){
            matrix[h][w] = 0;
        }
    }
    
    return matrix;
}

void matrix::initMatrixDiag(int** matrix, int width, int height){
    int diag = (width > height ) ? height : width;
    for (int d = 0; d < diag; d++){
        matrix[d][d] = d*d + d*d ;
    }
}

void matrix::printMatrix(int** matrix, int width, int height){
 
    
    for (int h = 0; h < height; h++){
        for (int w = 0 ; w < width; w++){
            std::cout<<std::right;
            std::cout<<std::setw(3);
            std::cout << matrix[h][w] << " ";
        }
        std::cout << std::endl;
    }
}
void matrix::cleanupMatrix(int** matrix, int height){
    for (int h = 0; h < height; h++){
        delete[] matrix[h];
    }

    delete[] matrix;
    matrix = 0;
}