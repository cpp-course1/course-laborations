#pragma once

#include <string>



template <typename T>
void print_info(T list[], int length);

void print_header();
void print_delimiter();

template <typename T>
void print_formatted_value(const T& value, const std::string& typeName);