#pragma once

#include <string>

namespace matrix {

    int** createMatrix(int width, int height);
    void printMatrix(int** matrix, int width, int height);
    void initMatrixDiag(int** matrix, int width, int height);
    void cleanupMatrix(int** matrix, int width);
}