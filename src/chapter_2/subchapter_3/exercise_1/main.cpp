#include <iostream>
#include <iomanip>
#include "matrix.h"

#include <string>


#include "main.h"
#include "utilities/utils.h"
#include "utilities/mathUtils.h"

const std::string& input_error_message = "Wrong format on input, I will not exit!! ";

int main(){

    int width, height;
    try{
        width = Utils::read_int_input("Enter a nubmer for the width of the matrix: ", input_error_message);
        height = Utils::read_int_input("Enter a nubmer for the height of the matrix: ", input_error_message);
    } 
    catch (std::invalid_argument& e){
        std::cerr << e.what() << std::endl;
        return 1;
    }

    int** matrix = matrix::createMatrix(width, height);

    matrix::initMatrixDiag(matrix, width, height);
    
    matrix::printMatrix(matrix, width, height);

    matrix::cleanupMatrix(matrix, height);

    return 0;
}
